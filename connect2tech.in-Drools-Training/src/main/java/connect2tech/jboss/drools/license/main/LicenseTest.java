package connect2tech.jboss.drools.license.main;

import java.io.IOException;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.jboss.drools.license.Applicant;

public class LicenseTest {

	public static void main(String[] args) throws DroolsParserException, IOException {
		LicenseTest droolsTest = new LicenseTest();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("License1");

		Applicant applicant = new Applicant();
		applicant.setAge(15);

		ksession.insert(applicant);
		ksession.fireAllRules();

		System.out.println(applicant.isValid());

		ksession.dispose();

	}

}