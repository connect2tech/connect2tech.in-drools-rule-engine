package connect2tech.jboss.drools.firealarm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

public class FireAlarmTest2 {

	public static void main(String[] args) throws DroolsParserException, IOException {
		FireAlarmTest2 droolsTest = new FireAlarmTest2();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("Alarm1");

		String[] names = new String[] { "kitchen", "bedroom", "office", "livingroom" };

		Map<String, Room> name2room = new HashMap<String, Room>();

		for (String name : names) {

			Room room = new Room(name);
			name2room.put(name, room);
			ksession.insert(room);
			Sprinkler sprinkler = new Sprinkler(room);
			ksession.insert(sprinkler);

		}

		Fire kitchenFire = new Fire(name2room.get("kitchen"));
		Fire officeFire = new Fire(name2room.get("office"));
		FactHandle kitchenFireHandle = ksession.insert(kitchenFire);
		FactHandle officeFireHandle = ksession.insert(officeFire);
		
		ksession.fireAllRules();
		
		//ksession.retract( kitchenFireHandle );
		//ksession.retract( officeFireHandle );
		//ksession.fireAllRules();

	}

}