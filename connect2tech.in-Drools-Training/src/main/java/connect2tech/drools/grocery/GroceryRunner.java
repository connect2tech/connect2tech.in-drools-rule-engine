package connect2tech.drools.grocery;

import java.io.IOException;
import java.math.BigDecimal;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.drools.grocery.ItemCity.City;
import connect2tech.drools.grocery.ItemCity.Type;

public class GroceryRunner {

	public static void main(String[] args) throws DroolsParserException, IOException {
		GroceryRunner droolsTest = new GroceryRunner();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("Grocery1");

		ItemCity item1 = new ItemCity();
		item1.setPurchaseCity(City.PUNE);
		item1.setTypeofItem(Type.MEDICINES);
		item1.setSellPrice(new BigDecimal(10));
		ksession.insert(item1);

		ItemCity item2 = new ItemCity();
		item2.setPurchaseCity(City.PUNE);
		item2.setTypeofItem(Type.GROCERIES);
		item2.setSellPrice(new BigDecimal(10));
		ksession.insert(item2);

		ItemCity item3 = new ItemCity();
		item3.setPurchaseCity(City.NAGPUR);
		item3.setTypeofItem(Type.MEDICINES);
		item3.setSellPrice(new BigDecimal(10));
		ksession.insert(item3);

		ItemCity item4 = new ItemCity();
		item4.setPurchaseCity(City.NAGPUR);
		item4.setTypeofItem(Type.GROCERIES);
		item4.setSellPrice(new BigDecimal(10));
		ksession.insert(item4);

		ksession.fireAllRules();

		System.out.println(item1.getPurchaseCity().toString() + " " + item1.getLocalTax().intValue());

		System.out.println(item2.getPurchaseCity().toString() + " " + item2.getLocalTax().intValue());

		System.out.println(item3.getPurchaseCity().toString() + " " + item3.getLocalTax().intValue());

		System.out.println(item4.getPurchaseCity().toString() + " " + item4.getLocalTax().intValue());

		ksession.dispose();

	}

}